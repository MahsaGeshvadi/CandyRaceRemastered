﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public class SoundList
{
    public string name;
    public Sound[] sounds;
    [HideInInspector] public Sound currentlyPlaying;

}
