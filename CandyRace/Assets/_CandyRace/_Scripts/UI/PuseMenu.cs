﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class PuseMenu : MonoBehaviour
{
    public static bool GameIsPaused = false;
    public GameObject PauseMenu;
    public static bool Quit = false;
    public GameObject Quitmenu;
    //public Button btnquit;

    // Update is called once per frame
    void Update()
    {
        /*if (EventSystem.current.IsPointerOverGameObject(Input.GetTouch(0).fingerId))
        {
            Debug.Log("Clicked on the UI");
        }*/
        if (Input.GetKeyDown(KeyCode.P))
            {
            if (GameIsPaused)
                Resume();
            else
                Pause();
        }

    }



    public void Resume()
    {
        PauseMenu.SetActive(false);
        Time.timeScale = 1f;

        AudioManager.Instance.PlayPlayingTrackFromList("BackGround");
        GameIsPaused = false;

    }

    public void Pause()
    {
        PauseMenu.SetActive(true);
        Time.timeScale = 0f;
        AudioManager.Instance.PausePlayingTrackFromList("BackGround");
        GameIsPaused = true;
    }

    public void LoadMenu()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene("MenuScene");
    }

    public void QuitMenu()
    {
        PauseMenu.SetActive(false);
        Quitmenu.SetActive(true);
        Time.timeScale = 0f;
        Quit = true;

    }

    public void quityes()
    {
        Application.Quit();
    }

    public void quitno()
    {
        PauseMenu.SetActive(true);
        Quitmenu.SetActive(false);
        Quit = false;
    }

}
