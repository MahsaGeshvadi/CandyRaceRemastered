﻿using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ObstacleObject
{
    public ObstacleType type;
    public GameObject ModelsParent;
    [HideInInspector]public List<GameObject> models;
}
