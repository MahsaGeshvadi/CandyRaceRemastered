﻿using System.Collections;
using UnityEngine;


public class Floor : MonoBehaviour
{
    public Material material;

    private void Start(){
        StartCoroutine(CalculateSpeed());
    }

    private IEnumerator CalculateSpeed(){
        while (true){
            SetFloorShaderSpeed(GameManager.Instance.GetEnvironmentSpeed() * GameManager.Instance.GetSpeedMultiplier());
            yield return new WaitForSeconds(0.5f);
        }
    }

    private void SetFloorShaderSpeed(float speed){
        material.SetFloat("_Speed", speed);
    }
}
