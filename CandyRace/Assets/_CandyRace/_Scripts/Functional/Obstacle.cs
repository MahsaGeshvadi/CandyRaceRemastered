﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;


public class Obstacle : MonoBehaviour
{
    public List<ObstacleObject> obstacleObjects;
    public float anotherSpeeedMultiplier = 1.1f;

    private ObstacleType type = ObstacleType.None;
    private Rigidbody rigidBody;
    private int activeModelIndex;
    private float speed;

    private void Awake(){
        SetDifferentModels();
        rigidBody = GetComponent<Rigidbody>();
    }

    private void SetDifferentModels(){
        foreach (ObstacleObject pm in obstacleObjects)
            for (int j = 0; j < pm.ModelsParent.transform.childCount; j++)
                pm.models.Add(pm.ModelsParent.transform.GetChild(j).gameObject);

    }

    private void OnDisable(){
        ObstacleObject selected = obstacleObjects.Find(x => x.type == type);
        if (type != ObstacleType.None)
            selected.models[activeModelIndex].SetActive(false);

        type = ObstacleType.None;
        rigidBody.velocity = new Vector3(0, 0, 0);
    }

    public void SetObstacleType(int P){
        OnDisable();

        type = (ObstacleType) P;
        // Debug.Log(P +" - "+type.ToString() );


        ObstacleObject selected = obstacleObjects.Find(x => x.type == type);
        activeModelIndex = Random.Range(0, selected.models.Count);
        selected.models[activeModelIndex].SetActive(true);
//        Debug.Log( this.GetComponent<Rigidbody>().GetInstanceID() );
        
    }

    private void Update(){
        speed = GameManager.Instance.GetEnvironmentSpeed() * GameManager.Instance.GetSpeedMultiplier() * anotherSpeeedMultiplier ;
        rigidBody.velocity = new Vector3(0, 0, -speed);
    }

    public ObstacleType GetObstacleType()
    {
        return type;
    }
}
