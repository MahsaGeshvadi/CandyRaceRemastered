﻿using UnityEngine;
using Random = UnityEngine.Random;


public class Rotator : MonoBehaviour
{
    public Vector3 speed;
    public bool worldRelativity = false;
    public bool getEffectFromGlobalSpeed;
    public bool directionRandomness;
    public float magnitudeRandomness;

    private Vector3 calculatedSpeed;


    private void OnEnable(){
        if (directionRandomness)
            speed = speed.magnitude * Random.insideUnitSphere.normalized;

        if (magnitudeRandomness > 0f)
            speed *= Random.Range(-magnitudeRandomness, magnitudeRandomness);

        CalculateSpeed();
    }

    private void CalculateSpeed(){

        if (getEffectFromGlobalSpeed)
            calculatedSpeed = GameManager.Instance.GetSpeedMultiplier() * GameManager.Instance.GetEnvironmentSpeed() * speed;
        else
            calculatedSpeed = speed;

    }

    private void Update(){
        CalculateSpeed();
        if (worldRelativity)
        {
            transform.Rotate(calculatedSpeed * Time.deltaTime,Space.World);
        }
        else
        {
            transform.Rotate(calculatedSpeed * Time.deltaTime);
        }
    }




}
