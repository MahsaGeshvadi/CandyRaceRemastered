﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCollider : MonoBehaviour
{
    // Start is called before the first frame update
    private void OnTriggerEnter(Collider other)
    {
        Obstacle obstacle = other.gameObject.GetComponent<Obstacle>();
        if (!obstacle)
            return;
        ObstacleType type = other.gameObject.GetComponent<Obstacle>().GetObstacleType();
        string functionName = "";
        functionName = type.ToString();
        functionName += "Collided";
        GameManager.Instance.Invoke(functionName, 0);
        other.gameObject.SetActive(false);
    }
}
