﻿using UnityEngine;


public class Move : MonoBehaviour
{
    public float horizontalSpeed = 10;
    public float rotationDragSpeed = 90f;
    public float rotationThreshold = 10;

    [Space] public GameObject frontWheelRight;
    public GameObject frontWheelLeft;
    public float wheelsExtraRotationMultiplier = 1.5f;

    private int lines;
    private int blockSize;
    private int lineSize;
    private float width;

    private Vector3 moveInp;
    private Vector3 rotateInp;


    private void Start()
    {
        lines = GameManager.Instance.GetLines();
        blockSize = GameManager.Instance.GetBlockSize();
        lineSize = GameManager.Instance.GetLineSize();
        width = this.GetComponent<BoxCollider>().size.x;
    }

    private void Update()
    {
        SetMoveAndRotationInputs();
        SetCarPosition();
        SetTilt();
    }

    private void SetMoveAndRotationInputs()
    {
        float inputHorizontal;
        #if UNITY_ANDROID && !UNITY_EDITOR
        inputHorizontal = Mathf.Clamp(Input.acceleration.x * 3,-1,1);
        #else
        inputHorizontal = Input.GetAxis("Horizontal");
        #endif
        moveInp = new Vector3(inputHorizontal, 0.0f, 0.0f);
        rotateInp = new Vector3(0.0f, inputHorizontal, 0.0f);
    }

    private void SetCarPosition()
    {
        transform.position = new Vector3(
            Mathf.Clamp(
                transform.position.x + (moveInp.x * horizontalSpeed * Time.deltaTime),
                (-(lines / 2) * blockSize * lineSize) + width / 2, ((lines / 2) * blockSize * lineSize) - width / 2)
            , 0
            , 0);
    }

    private void SetTilt()
    {
        float rotY = transform.eulerAngles.y;
        if (rotY > 180 && rotY <= 360)
            rotY -= 360;
        rotY = Mathf.Lerp(rotY, rotationThreshold * rotateInp.y, (Mathf.Abs(rotateInp.y) <= 0.1f ? 1 : Mathf.Abs(rotateInp.y)) * rotationDragSpeed * Time.deltaTime);
        Vector3 result = new Vector3(0.0f, rotY, 0.0f);
        transform.eulerAngles = result;

        frontWheelLeft.transform.localEulerAngles = result * wheelsExtraRotationMultiplier;
        frontWheelRight.transform.localEulerAngles = result * wheelsExtraRotationMultiplier;
    }
}
