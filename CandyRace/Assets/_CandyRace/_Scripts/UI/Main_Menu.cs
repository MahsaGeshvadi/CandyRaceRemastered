﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Main_Menu : MonoBehaviour
{
    public AudioMixerGroup mainMixer;
    public Slider audioSlider;
    public GameObject Quitmenu;
    public static bool Quit = false;
    public GameObject MainMenu;

    private void Start()
    {
        if (!mainMixer || !audioSlider)
            return;
        mainMixer.audioMixer.GetFloat("MainVolume", out float val);
        audioSlider.value = Remap(val, new Vector2(-80, 10), new Vector2(0, 1));
    }

    public void playgame()
    {
        //SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex+1);
        Time.timeScale = 1f;
        SceneManager.LoadScene("MainScene");
    }

    public void LoadMenu()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene("MenuScene");
    }

    public void QuitMenu()
    {
        MainMenu.SetActive(false);
        Quitmenu.SetActive(true);
        Time.timeScale = 0f;
        Quit = true;
    }

    public void quityes()
    {
        Application.Quit();
    }

    public void quitno()
    {
        MainMenu.SetActive(true);
        Quitmenu.SetActive(false);
        Quit = false;
    }

    public void AudioValueChanged()
    {
        if (mainMixer && audioSlider)
            mainMixer.audioMixer.SetFloat("MainVolume", Remap(audioSlider.value, new Vector2(0, 1), new Vector2(-80, 10)));
    }

    public static float Remap(float value, Vector2 oldRange, Vector2 newRange)
    {
        value = Mathf.Clamp(value, oldRange.x, oldRange.y);
        return newRange.x + (value - oldRange.x) * (newRange.y - newRange.x) / (oldRange.y - oldRange.x);
    }
}
