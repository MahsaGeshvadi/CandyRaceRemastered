﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using Random = UnityEngine.Random;

public class AudioManager : MonoBehaviour
{
    public AudioMixerGroup mixer;
    public SoundList[] soundLists;

    


    public static AudioManager Instance { get; private set; }

    private void Awake(){
        #region Singletone
        if (Instance != null && Instance != this){
            Destroy(gameObject);
        }
        else{
            Instance = this;
        }
        #endregion

        foreach (var soundList in soundLists)
        {
            foreach (var sound in soundList.sounds)
            {
                sound.source = gameObject.AddComponent<AudioSource>();
                sound.source.clip = sound.clip;
                sound.source.outputAudioMixerGroup = mixer;

                sound.source.volume = sound.volume;
                sound.source.pitch = sound.pitch;
                sound.source.loop = sound.loop;
            }
        }
    }

    private void Start()
    {
        PlayRandomFromList("BackGround");
    }

    public void PlayRandomFromList(string listName)
    {
        SoundList sl = Array.Find(soundLists, soundlist => soundlist.name ==listName);
        if (sl == null)
        {
            // Debug.Log("sound list not found 404");
            return;
        }

        Sound s = sl.sounds[Random.Range(0,sl.sounds.Length)];
        sl.currentlyPlaying = s;
        s.source.Play();
    }
    
    public void StopPlayingTrackFromList(string listName)
    {
        SoundList sl = Array.Find(soundLists, soundlist => soundlist.name == listName);
        if (sl == null)
        {
            // Debug.Log("sound list not found 404");
            return;
        }
        sl.currentlyPlaying.source.Stop();
    }
    public void PausePlayingTrackFromList(string listName)
    {
        SoundList sl = Array.Find(soundLists, soundlist => soundlist.name == listName);
        if (sl == null)
        {
            // Debug.Log("sound list not found 404");
            return;
        }
        sl.currentlyPlaying.source.Pause();
    }
    public void PlayPlayingTrackFromList(string listName)
    {
        SoundList sl = Array.Find(soundLists, soundlist => soundlist.name == listName);
        if (sl == null)
        {
            // Debug.Log("sound list not found 404");
            return;
        }
        sl.currentlyPlaying.source.Play();
    }
    public void Loop(string name, bool loop)
    {

    }

    public void IncreasePitch(float amount,string listName)
    {
        SoundList sl = Array.Find(soundLists, soundlist => soundlist.name ==listName);
        if(sl==null)
            return;
        Debug.Log(amount);
        sl.currentlyPlaying.source.pitch += amount;
    }

}
