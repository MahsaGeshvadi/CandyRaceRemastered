﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameOver : MonoBehaviour
{
    public GameObject Gameover;
    public static bool GameIsOver = false;
    void Start()
    {
        
    }
    public void Gameoveer()
    {
        Gameover.SetActive(true);
        Time.timeScale = 0f;
        GameIsOver = true;
    } 
   
    public void LoadMenu()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene("menu");
    }

}
