﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using TMPro;
using UnityEngine;
using Random = UnityEngine.Random;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class GameManager : MonoBehaviour
{
    [SerializeField] private int lines = 4;

    [SerializeField] private int blockSize = 2;

    // every size are calculated by blockSize
    [SerializeField] private int lineSize = 2;

    [Space] [SerializeField] private int numberOfWaves = 5;

    //distance between waves in block translations
    [SerializeField] private int waveDistance = 200;
    [SerializeField] [Range(0, 1)] private float chanceOfEmptyWave;

    [Space]
    //environment speed from 1 to 16
    [SerializeField]
    [Range(1, 16)]
    private int environmentSpeed = 1;

    [SerializeField] private float speedIncreasingAmount = 0.2f;
     private float pitchIncreasingAmount = 0.015f;

    [Space] [SerializeField] private Obstacle obstacle;
    [Space] [SerializeField] private TextMeshProUGUI scoreText;
    [Space] [SerializeField] private TextMeshProUGUI gameOverScoreText;
    [SerializeField] private TextMeshProUGUI difficultyText;
    [SerializeField] private TextMeshProUGUI healthText;
    [SerializeField] private TextMeshProUGUI scoreUpText;

    //difficulty of game from 1 to 128
    private float speedMultiplier = 5;

    private int difficulty = 1;

    // threshold indicates how much difference effects the game
    private readonly int scoreThreshold = 250;
    private readonly int difficultyThreshold = 1;

    private int health = 3;
    private int score;


    //road Length calculated in awake in block size
    private int roadLength;
    private readonly Dictionary<ObstacleType, int> frequencies = new Dictionary<ObstacleType, int>();
    private readonly float scoringTimeSeconds = 0.05f;
    private readonly float speedIncreasingDelayTimeSeconds = 0.5f;
    private readonly Queue<Obstacle> obstacles = new Queue<Obstacle>();
    private int obstacleCount;

    public GameObject GameOver;
    public GameObject exitPanel;


    #region Singletone


    public static GameManager Instance { get; private set; }

    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(gameObject);
        }
        else
        {
            Instance = this;
        }

        Init();
    }

    #endregion


    #region getters

    public float GetSpeedMultiplier()
    {
        return speedMultiplier;
    }

    public int GetLines()
    {
        return lines;
    }

    public int GetBlockSize()
    {
        return blockSize;
    }

    public int GetLineSize()
    {
        return lineSize;
    }

    public int GetEnvironmentSpeed()
    {
        return environmentSpeed;
    }

    #endregion


    #region DynamicFuncs

    private void Init()
    {
        roadLength = numberOfWaves * (waveDistance + 1);
        obstacleCount = lines * (numberOfWaves + 1);
        InstantiateObstacles();
        SetFrequencies();
        StartCoroutine(ScoringThroughTime());
//        StartCoroutine(IncreasingSpeedThroughTime());
        StartCoroutine(MakeWaves());
        UpdateHealthText();
        UpdateScoreText();
        UpdateDifficultyText();
        HideScoreUpText();
    }

    private void InstantiateObstacles()
    {
        GameObject parentHolder = new GameObject();
        parentHolder.transform.position = new Vector3(0, 0, 0);
        parentHolder.name = "ObstaclesParent";

        for (int i = 0; i < obstacleCount; i++)
        {
            Obstacle tempObstacle = Instantiate(obstacle, parentHolder.transform, false);
            tempObstacle.gameObject.SetActive(false);
            obstacles.Enqueue(tempObstacle);
        }
    }

    private void SetFrequencies()
    {
        for (int i = 1; i <= Enum.GetNames(typeof(ObstacleType)).Length; i++)
        {
            ObstacleType temp = (ObstacleType) i;
            frequencies.Add(temp, 0);
        }

        frequencies[ObstacleType.Block] = 60;
        frequencies[ObstacleType.Score] = 25;
        frequencies[ObstacleType.Life] = 1;
    }

    private IEnumerator MakeWaves()
    {
        while (true)
        {
            yield return new WaitForSeconds((waveDistance * blockSize) * 1.0f / (speedMultiplier * environmentSpeed)); // todo : ask asha for block size
            MakeWave();
        }
    }

    private void MakeWave()
    {
        if (Random.value > chanceOfEmptyWave)
        {
            int waveDetail = Random.Range(1, (int) Math.Pow(2, lines));
            string waveDetailBits = Convert.ToString(waveDetail, 2);

            for (int i = 0; i < waveDetailBits.Length; i++)
                if (waveDetailBits[i] == '1')
                    InsertObstacle(i);
        }
    }

    private void InsertObstacle(int lineNumber)
    {
        Vector3 position = new Vector3();

        position.z = roadLength * blockSize;

        int mid = lines / 2;
        position.x = (lines % 2 == 1 ? 0 : blockSize * lineSize / 2) + (lineNumber - mid) * blockSize * lineSize;
        position.y = blockSize / 4f;

        position.x += Random.Range(-lineSize / 2f, lineSize / 2f) * blockSize;
        Obstacle temp = obstacles.Dequeue();
        temp.transform.position = position;
        temp.SetObstacleType(SetObstacleType());
        temp.gameObject.SetActive(true);
        obstacles.Enqueue(temp);
    }

    private int SetObstacleType()
    {
        int res = 0;
        int sum = 0;
        foreach (var frequency in frequencies)
            sum += frequency.Value;


        int random = Random.Range(0, sum);
        int temp = 0;
        foreach (var frequenty in frequencies)
        {
            temp += frequenty.Value;
            if (random < temp)
            {
                res = (int) frequenty.Key;
//                Debug.Log(" sum " + sum + " res "+ res+" rand " + random + " type " + (int)frequenty.Key);
                break;
            }
        }

        return res;
    }

    private void Lost()
    {
        AudioManager.Instance.StopPlayingTrackFromList("BackGround");
        // Debug.Log("lost....");
        GameOver.SetActive(true);
        gameOverScoreText.text = score + "";
        Time.timeScale = 0f;
    }

    public int ScoreUp(int add)
    {
        int amount = (add * difficulty);
        score += amount;
        UpdateScoreText();
            SetDifficulty(CalculateDifficultyBasedOnScore());
        return amount;
    }

    private int CalculateDifficultyBasedOnScore()
    {
        int sum = 0;
        for (int i = 1; ; i++)
        {
            sum += i * scoreThreshold;
            if (sum > score)
                return i;
        }
    }

    private void SetDifficulty(int value)
    {
        if(difficulty==value)
            return;
        difficulty = value;
        difficulty = Mathf.Clamp(difficulty, 1, 123);
        UpdateDifficultyText();
        if (difficulty % difficultyThreshold == 0)
        {
            IncreasingSpeed();
        }
    }


    private void DifficultyDown()
    {
        if (difficulty > 1)
        {
            difficulty--;
        }
    }

    private void Shake()
    {
    }

    IEnumerator ScoringThroughTime()
    {
        while (true)
        {
            yield return new WaitForSeconds(scoringTimeSeconds);
            ScoreUp(1);
        }
    }

    private void HealthDown()
    {
        health--;
        UpdateHealthText();
        if (health == 0)
        {
            Lost();
        }
    }

    private void HealthUp()
    {
        health++;
        UpdateHealthText();
    }

    private void UpdateHealthText()
    {
        healthText.text = "X " + health;
    }

    private void UpdateScoreText()
    {
        scoreText.text = score + "";
    }

    private int scoreUpTextShowing;

    private void UpdateScoreUpText(int value)
    {
        scoreUpTextShowing++;
        scoreUpText.gameObject.SetActive(true);
        scoreUpText.text = value / scoreUpTextShowing + "  X " + scoreUpTextShowing;
        CancelInvoke(nameof(HideScoreUpText));
        Invoke(nameof(HideScoreUpText), 1.5f);
    }

    private void HideScoreUpText()
    {
        scoreUpText.gameObject.SetActive(false);
        scoreUpTextShowing = 0;
    }

    private void UpdateDifficultyText()
    {
        difficultyText.text = difficulty + "";
    }

    private void IncreasingSpeed()
    {
        speedMultiplier += speedIncreasingAmount;

        AudioManager.Instance.IncreasePitch(pitchIncreasingAmount,"BackGround");
    }

    #endregion


    #region Collided

    public void LifeCollided()
    {
        HealthUp();
        AudioManager.Instance.PlayRandomFromList("Score");
//        Debug.Log("life collided");
    }

    public void BlockCollided()
    {
        HideScoreUpText();
        Shake();
        DifficultyDown();
        HealthDown();
        AudioManager.Instance.PlayRandomFromList("Block");
        //        Debug.Log("block collided");
    }

    public void ScoreCollided()
    {
        int amount = ScoreUp(10 * (scoreUpTextShowing + 1));
        UpdateScoreUpText(amount);
        AudioManager.Instance.PlayRandomFromList("Score");
//        Debug.Log("score collided");
    }

    #endregion


    public void ShowExitMenu()
    {
        exitPanel.SetActive(true);
    }

    public void HideExitMenu()
    {
        exitPanel.SetActive(false);
    }
}
